import 'bootstrap/dist/css/bootstrap.min.css';
import { useEffect } from 'react';
import AOS from 'aos';
import 'aos/dist/aos.css';
import './App.css';
import {BrowserRouter,Routes , Route } from "react-router-dom";
import FreeQuote from "./Modules/Pages/Free Quote/FreeQuote";
import Layout from './Modules/Layouts/Layout';
import OurTeam from './Modules/Pages/Our Team/OurTeam';
import About from './Modules/Pages/About/About';
import Service from './Modules/Pages/Service/Service';
import HomePage from './Modules/Home/Home';
import Contact from '../src/Modules/Contact/Contact';
import ErrorPage from '../src/Modules/Pages/404page/ErrorPage';
import Features from "./Modules/Pages/Features/Features";
import MainTestimonial from "./Modules/Pages/Testimonial/Main-Testimonial";
// ..
AOS.init();



function App() {
  useEffect(() => {
    AOS.init();
  }, []);

  return (
    <BrowserRouter>
      <Routes>
      <Route path='/' element={<Layout/>}>
        <Route index element={<HomePage/>}/>
        <Route path="about" element={<About/>}/>
        <Route path="service" element={<Service/>}/>
        <Route path="pages" element="">
          <Route path="/pages/feature" element={<Features/>}/>
          <Route path="/pages/free-quote" element={<FreeQuote/>}/>
          <Route path="/pages/our-team" element={<OurTeam/>}/>
          <Route path="/pages/testimonial" element={<MainTestimonial/>}/>
          <Route path="/pages/404-pages" element={<ErrorPage/>}/>
        </Route>
        <Route path="contact" element={<Contact/>}/>
        </Route>
      </Routes>
    </BrowserRouter>
  );
}
export default App;
