import React from 'react'

export const Title = (props) => {
  return (
    <div className="mb-3">
    <h2 className='text-dark'>{props.title}</h2>
  </div>
  )
}
