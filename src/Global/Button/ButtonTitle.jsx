import React from "react";

function ButtonTitle(props) {
  return (
      <div
      className="d-inline-block border rounded-pill text-primary px-4 mb-3 "
    >
      {props.name}
    </div>
  );
}

export default ButtonTitle;
