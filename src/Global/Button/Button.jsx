import React from 'react';
import {Button} from "react-bootstrap";

function Buttons(props) {
    return (
        <>
            <Button className='btn bg-primary border-2 border-light rounded-pill py-3 px-5 mt-2' >Read More</Button>
        </>
    );
}
export default Buttons;