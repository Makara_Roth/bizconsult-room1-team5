import { Container } from "react-bootstrap"
import { Link } from "react-router-dom"

const HeroSection = (props) => {
  const {title, subtitle} = props

  return (
    <Container className="p-0 heroSection" >
      <div className="d-flex flex-column align-items-center justify-content-center bg-primary text-white" style={{minHeight: "290px" }}>
        <h1 className="text-white mb-3 font-bold">{title}</h1>
        <div className=" d-inline-flex text-white">
        <p className="m-0">
          <Link to="/" className="text-white text-decoration-none">
            Home         
          </Link>
        </p>
        <p className="m-0 px-2">/</p>
        <p className="m-0">
          <Link to="" className="text-white text-decoration-none">
            Pages
          </Link>
        </p>
        <p className="m-0 px-2">/</p>
        <p className="m-0">
          {subtitle}
        </p>
        </div>
      </div>
    </Container>
  )
}

export default HeroSection
