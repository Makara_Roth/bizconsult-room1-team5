
import React from 'react'
import Header from './Header/Header';
import { Outlet } from 'react-router-dom';
import Footer from './Footer/Footer';
import BackToTop from "../BackToTop/BackToTop";

const Layout = () => {
    return (
        <>
            <Header />
            <BackToTop/>
            <Outlet />
            <Footer/>

        </>
    )
}

export default Layout