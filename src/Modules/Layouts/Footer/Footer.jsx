import "../../../Assets/css/Footer/Footer.css"
import GetInTech from "../../Components/Footers/Get_In_Touch";
import Quicklink from "../../Components/Footers/Quick_link";
import PopularLink from "../../Components/Footers/Popular_Link";
import NewsLetter from "../../Components/Footers/News_Letter";
import FooterTop from "../../Components/Footers/FooterTop";


const Footer = () => {
    return (
        <>
            <div className="container mt-5 pt-5 bg-dark" data-aos="zoom-in" data-aos-duration="1000">
                <div className="row mb-5">
                    <div className={'col-md-5 col-lg-3'}>
                        <GetInTech/>
                    </div>
                    <div className={'col-md-5 col-lg-3'}>
                        <Quicklink/>
                    </div>
                    <div className={'col-md-5 col-lg-3'}>
                        <PopularLink/>
                    </div>
                    <div className={'col-md-5 col-lg-3'}>
                        <NewsLetter/>
                    </div>
                </div>
                <hr className={'text-white'}/>
                <div className={'p-5 mt-3 pt-3'}>
                    <FooterTop/>
                </div>
            </div>
        </>
    )
};
export default Footer;