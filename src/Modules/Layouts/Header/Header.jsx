import 'bootstrap/dist/css/bootstrap.min.css';
import {Navbar, Nav, NavDropdown, Button, NavLink} from 'react-bootstrap';
import {Link} from "react-router-dom";
import "../../../Assets/css/header/header.css";
import {useState} from 'react';
import { LinkContainer } from 'react-router-bootstrap'
function Header () {
    const [navbar, setNavbar] = useState(false);
    const [show, setShow] = useState(false);

    const changeBackground = () => {
        if (window.scrollY >= 80) {
            setNavbar(true)
        }else {
            setNavbar(false)
        }
    }
    window.addEventListener('scroll', changeBackground);
    const showDropdown = (e)=>{
        setShow(!show);
    }
    const hideDropdown = e => {
        setShow(false);
    }

    return (
        <>
            <Navbar collapseOnSelect expand="lg" className={`navbar bg-style px-4 px-lg-5 py-3 py-lg-0 sticky-top ${navbar ? 'navbar active container-fluid shadow' : 'navbar container'}`}>
                    <h1 className="me-auto ms-4 logo" as={Link} to={"/"}>BizConsult</h1>
                    <Navbar.Toggle className="text-white" aria-controls="responsive-navbar-nav"/>
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="nav-item ms-auto m-3 mx-5">

                            <LinkContainer  as={Link} to={"/"}>
                                <NavLink className="nav-item fw-bold me-3 ">Home</NavLink>
                            </LinkContainer>
                            <LinkContainer  as={Link} to={"/about"}>
                                <NavLink className="nav-item fw-bold me-3" >About</NavLink>
                            </LinkContainer>
                            <LinkContainer as={Link} to={"/service"}>
                                <NavLink className="nav-item fw-bold me-3" >Service</NavLink>
                            </LinkContainer>
                            <NavDropdown title="Pages" className="nav-item dropdown" id="collasible-nav-dropdown"
                            show={show}
                            onMouseEnter={showDropdown} 
                            onMouseLeave={hideDropdown}
                            >
                                <LinkContainer as={Link} to={"/pages/feature"}>
                                    <NavDropdown.Item>Features</NavDropdown.Item>
                                </LinkContainer>
                               <LinkContainer as={Link} to={"/pages/free-quote"}>
                                   <NavDropdown.Item >Free Quote</NavDropdown.Item>
                               </LinkContainer>
                                <LinkContainer as={Link} to={"/pages/our-team"}>
                                    <NavDropdown.Item >Our Team</NavDropdown.Item>
                                </LinkContainer>
                                <LinkContainer as={Link} to={"/pages/testimonial"}>
                                    <NavDropdown.Item >Testimonial</NavDropdown.Item>
                                </LinkContainer>
                                <LinkContainer as={Link} to={"/pages/404-pages"}>
                                    <NavDropdown.Item >404 Page</NavDropdown.Item>
                                </LinkContainer>
                            </NavDropdown>
                            <LinkContainer  as={Link} to={"/contact"}>
                                <NavLink className="nav-item fw-bold me-4">Contact</NavLink>
                            </LinkContainer>
                            <Link to="/freequote">
                                <Button className="btn-freequote rounded-pill" variant="light" as={Link} to={"/pages/free-quote"}>Free
                                    Quote</Button>
                            </Link>
                        </Nav>
                    </Navbar.Collapse>
            </Navbar>    
        </>
    );
}

export default Header