import Hero from "../Components/Hero/Hero";
import AboutCard from "../Components/About/AboutCard";
import {ReadyToGetStart} from "../Components/Ready-to-get-started/ReadyToGetStart";
import ServiceCard from "../Components/Service/ServiceCard";
import Feature from "../Components/Features/Feature";
import Testimonial from "../Components/Testimonial/Testimonial";
import ClientsSupportSwiper from "../Components/ClientsSupport/ClientsSupportSwiper";
import OurTeamCard from "../Components/OurTeam/OurTeamCard";

const HomePage = () => {
    return (
        <>
            <Hero/>
            <AboutCard/>
            <ReadyToGetStart/>
            <ServiceCard/>
            <Feature/>
            <ClientsSupportSwiper/>
            <Testimonial/>
            <OurTeamCard/>
        </>
    )
}
export default HomePage;