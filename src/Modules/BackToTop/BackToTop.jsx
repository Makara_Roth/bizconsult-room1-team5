import React, {useEffect, useState} from 'react';
import "../../Assets/css/BackToTop/BackToTop.css"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowUp} from "@fortawesome/free-solid-svg-icons";


function BackToTop(props) {
    const [showToTopBtn , setShowTopBtn ] = useState(false);

    const goToTop = () => {
        window.scrollTo({
            top: 0,
            behavior: 'smooth',
        });
    };
    useEffect(() => {
        window.addEventListener('scroll', () => {
            if (window.scrollY > 400) {
                setShowTopBtn(true);
            } else {
                setShowTopBtn(false);
            }
        });
    }, []);
    return (
        <div className={'top-to-btm'}>
            { " " }
            {
                showToTopBtn && (
                <div className={'icon-position icon-style btn-primary '} onClick={goToTop} >
                    <FontAwesomeIcon icon={faArrowUp} className={'btn-lg'} />
                </div>
                    )}
            {" "}
        </div>
    );
}

export default BackToTop;