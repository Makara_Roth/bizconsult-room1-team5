
import ContactPage from './components/ContactPage'
import HeroSection from '../../Global/Hero/HeroSection'

const Contact = () => {
  return (
    <>
       <HeroSection title="Contact" home="Home" pages="Pages" subtitle="Contact"/>
        <ContactPage/>
    </>
  )
}

export default Contact


