
import Button from 'react-bootstrap/Button';
import React from 'react';
import Form from 'react-bootstrap/Form';
import ButtonTitle from '../../../Global/Button/ButtonTitle';
import { Title } from '../../../Global/Title/Title';

function ContactPage() {
  return (
    <div className='container' style={{maxWidth:"50rem"}}>
      <div className='d-flex justify-content-center my-1 mt-5 '>
        <ButtonTitle name={" Contact "} />
      </div>
      <div className='d-flex justify-content-center text-center text-dark mt-3'>
        <Title title="If You Have Any Query, Please Feel Free Contact Us" />

      </div>
      <Form style={{ marginTop: '3rem' }}>
        <div  className='container'>
        <div className='row'>
          <div className='col-lg-6'>
            <Form.Group className="mb-3" controlId="formBasicName">

              <Form.Control type="Name" placeholder="Your Name" size="lg" />

            </Form.Group>
          </div>
          <div className='col-lg-6'>
            <Form.Group className="mb-3" controlId="formBasicEmail" >

              <Form.Control type="email" placeholder="Enter email" size="lg" />

            </Form.Group>
          </div>

          <Form.Group className="mb-3" controlId="formBasicSubject" >

            <Form.Control type="Subject" placeholder="Subject" size="lg" />
          </Form.Group>
          <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">

            <Form.Control as="textarea" rows={3} placeholder="Message" size="lg" />
          </Form.Group>
          <Button variant="primary" type=" Send Message" size="lg" >
            Send Message
          </Button>
        </div>
        </div>
      </Form>
    </div>
  );
}

export default ContactPage;


