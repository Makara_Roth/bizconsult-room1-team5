import AboutCard from "../../Components/About/AboutCard";
import "./About.css";
import HeroSection from "../../../Global/Hero/HeroSection";
import Feature from "../../Components/Features/Feature";
import OurTeamCard from "../../Components/OurTeam/OurTeamCard";

export default function About() {

  return (
    <div>
      <HeroSection title="About Us" home="Home" pages="Pages" subtitle="About"/>
      <AboutCard />
      <Feature/>
      <OurTeamCard/>
    </div>
  );
}
