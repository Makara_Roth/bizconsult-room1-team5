import ErrorPages from '../../Components/404Page/ErrorPages'
import HeroSection from '../../../Global/Hero/HeroSection'

const ErrorPage = () => {

  return (
   <>
     <HeroSection title="Not Found" home="Home" pages="Pages" subtitle="404"/>
       <ErrorPages/>
      </>
  )
}

export default ErrorPage