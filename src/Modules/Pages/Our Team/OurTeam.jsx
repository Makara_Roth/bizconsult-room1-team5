import HeroSection from "../../../Global/Hero/HeroSection";
import "./OurTeam.css";
import CardItem from "../../Components/OurTeam/OurTeamCard";

const OurTeam = () => {
  return (
    <>
        <HeroSection title="Our Team" home="Home" pages="Pages" subtitle="Our Team"/>
        <CardItem/>
    </>
  );
};

export default OurTeam;
