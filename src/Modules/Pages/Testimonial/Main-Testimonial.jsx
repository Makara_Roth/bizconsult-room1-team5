import React from 'react';
import HeroSection from "../../../Global/Hero/HeroSection";
import Testimonial from "../../Components/Testimonial/Testimonial";

function MainTestimonial(props) {
    return (
   <>
       <HeroSection title="Testimonial" home="Home" pages="Pages" subtitle="Testimonial"/>
       <Testimonial/>
   </>
    );
}

export default MainTestimonial;