import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import ButtonTitle from '../../../Global/Button/ButtonTitle';
import Row from 'react-bootstrap/Row';

import { Title } from '../../../Global/Title/Title';

function FreeQuotes() {
  return (
    <div className='container ' style={{maxWidth:"50rem"}}>
       <div className='d-flex justify-content-center my-1 mt-5 '>
        <ButtonTitle name={" Free Quotes "}/>
      </div>
     <div className='d-flex justify-content-center text-center text-dark mt-3'>
     <Title title="Request A Free Quote"/>
     </div>
     <Form>
     <div className='row'>
      <Row className="mb-3">
      <div className='col-lg-6'>
            <Form.Group className="mb-3" controlId="formBasicName">

              <Form.Control type="Name" placeholder="Your Name" size="lg" />

            </Form.Group>
          </div>
          <div className='col-lg-6'>
            <Form.Group className="mb-3" controlId="formBasicEmail" >

              <Form.Control type="email" placeholder="Enter email" size="lg" />

            </Form.Group>
          </div>
      </Row>
      <div className='mb-3'>
    <Form.Select aria-label="Default select example"size="lg" >
      <option>Financial Consultancy</option>
      <option value="1">Strategy Consultancy</option>
      <option value="2">Tax Consultancy</option>
    </Form.Select>
    </div>
    <Form.Group className="mb-3">
        <Form.Control as="textarea" rows={3} />
      </Form.Group>
      <Button variant="primary" type="submit"size="lg" >
        Submit
      </Button>
      </div>
    </Form>
  
    </div>
  );
}

export default FreeQuotes;