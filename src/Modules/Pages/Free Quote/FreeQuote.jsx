
import FreeQuotes from './FreeQuotes';
import HeroSection from '../../../Global/Hero/HeroSection';

const FreeQuote = () => {

  return (
  
      <>
       <HeroSection title="Free Quotes " home="Home" pages="Pages" subtitle="Free Quotes"/>

       <FreeQuotes/>

      </>
  )
  
}

export default FreeQuote