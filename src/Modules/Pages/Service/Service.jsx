import HeroSection from "../../../Global/Hero/HeroSection";
import ServiceCard from "../../Components/Service/ServiceCard";
import Testimonial from "../../Components/Testimonial/Testimonial";

const Service = () => {
    return (
        <>
            <HeroSection title="Services" subtitle="Services"/>
            <ServiceCard/>
            <Testimonial/>
        </>
    )
}

export default Service;
