import Feature from "../../Components/Features/Feature";
import HeroSection from "../../../Global/Hero/HeroSection";

function Features(props) {
    return (
        <>
            <HeroSection title="Features" home="Home" pages="Pages" subtitle="Features"/>
            <Feature/>
        </>
    );
}

export default Features;