import React from 'react'
import { Button, Col, Container, Row } from 'react-bootstrap'
import newsletter from "../../../Assets/img/newsletter.png"

export const ReadyToGetStart = () => {
  return (
    <Container className='mt-5 p-0'>
      <div className='bg-primary my-6'>
        <Row style={{height: "250px"}} className=' align-items-center m-5'>
          <Col md={6}>
            <h3 className='text-white'>Ready To get started</h3>
            <small className='text-white'>Diam elitr est dolore at sanctus nonumy.</small>
            <div className=' position-relative'>
              <input className='w-100 form-control border-0 rounded-pill pe-5 ps-4 mt-3' placeholder='Enter Your Email' style={{height: "48px"}} />
              <Button className=' position-absolute top-0 end-0 mt-1 me-2 shadow-none bg-white border-0'><i className="fa fa-paper-plane text-primary fs-4"></i></Button>
            </div>
          </Col>
          <Col md={6} className='text-center'>
            <img className=' img-fluid mt-5' src={newsletter} alt='--' style={{maxHeight: "250px"}}/>
          </Col>
        </Row>
      </div>
    </Container>
  )
}
