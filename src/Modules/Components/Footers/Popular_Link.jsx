import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChevronRight} from "@fortawesome/free-solid-svg-icons";

const Popular_Link = () => {
    return (
        <div className={'text-white mx-5 '}>
            <h5 className={'text-white mt-2 '}>Popular Link</h5>
            <div className={'py-3'} style={{fontSize: "15px"}}>
                <p className={'btn-link text-decoration-none py-0 text-white  mx-auto'}><FontAwesomeIcon icon={faChevronRight}/> About US</p>
                <p className={'btn-link text-decoration-none py-0 text-white  mx-auto'}><FontAwesomeIcon icon={faChevronRight}/> Contact Us </p>
                <p className={'btn-link text-decoration-none py-0 text-white  mx-auto'}><FontAwesomeIcon icon={faChevronRight}/> Privacy Policy </p>
                <p className={'btn-link text-decoration-none py-0 text-white  mx-auto'}><FontAwesomeIcon icon={faChevronRight}/> Teams & Condition</p>
                <p className={'btn-link text-decoration-none py-0 text-white  mx-auto'}><FontAwesomeIcon icon={faChevronRight}/> Career</p>
            </div>
        </div>
    );
};

export default Popular_Link;
