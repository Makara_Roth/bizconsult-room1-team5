import {Link} from "react-router-dom";

function FooterTop(props) {
    return (
        <div className={'row footerTop text-white'}>
            <div className={'col-md-6 text-center text-md-start mb-3 mb-md-0 text-white'}>
                ©
                <Link to={"/"}> BizConsult</Link>
                All Right Reserved.
            </div>
            <div className={'col-md-6  text-center text-md-end mx-md-auto-auto'}>
                    <Link to={"/"} className={'footerTop_menu text-decoration-none m-2 pe-3'}>Home</Link>
                    <Link to={"/"} className={'footerTop_menu text-decoration-none m-2 pe-3'}>Cookies</Link>
                    <Link to={"/"} className={'footerTop_menu text-decoration-none m-2 pe-3'}>Help</Link>
                    <Link to={"/"} className={' text-decoration-none text-white m-2 pe-3'}>FQAs</Link>
            </div>
        </div>
    );
}

export default FooterTop;