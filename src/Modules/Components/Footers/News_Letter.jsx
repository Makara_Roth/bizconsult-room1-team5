import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import { faPaperPlane } from "@fortawesome/free-solid-svg-icons";
function NewsLetter(props) {
    return (
        <div className={'text-white mx-5 '}>
            <h5 className={'text-white mt-2'}>Quick Link </h5>
            <p style={{fontSize:"15px"}} >Lorem ipsum dolor sit amet elit. Phasellus nec pretium mi. Curabitur facilisis ornare velit non vulpu</p>
            <div className={'position-relative w-100 mt-3'}>
                <input type={'text'} placeholder={'Your Email'} className={'form-control border-0 rounded-pill  w-100 ps-4 pe-5'}/>
                <button className={'btn shadow-none position-absolute top-0 end-0 mt-0 me-3 text-primary'} ><FontAwesomeIcon icon={faPaperPlane}/></button>
            </div>
        </div>
    );
}

export default NewsLetter;