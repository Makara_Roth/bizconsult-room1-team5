import React from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import { faMapMarker , faPhone , faMailBulk  } from "@fortawesome/free-solid-svg-icons";
import {faFacebook ,faXTwitter , faYoutube ,faInstagram , faLinkedin } from "@fortawesome/free-brands-svg-icons"

function Get_In_Touch(props) {
    return (
        <>
            <div className={'text-white mx-5 '}>
                <h5 className={'text-white mt-2 my-4 fw-bold'}>Get In Touch </h5>
                <p style={{fontSize:"15px"}}><FontAwesomeIcon icon={faMapMarker}/> 123 Street, New ,York , USA </p>
                <p style={{fontSize:"15px"}}><FontAwesomeIcon icon={faPhone}/> +012 345 67890 </p>
                <p style={{fontSize:"15px"}}><FontAwesomeIcon icon={faMailBulk}/> info@example.com</p>
                <div className={'d-flex py-3 '}>
                    <FontAwesomeIcon icon={faXTwitter} className={'btn btn-outline-light btn-social'}/>
                    <FontAwesomeIcon icon={faFacebook} className={'btn btn-outline-light btn-social'}/>
                    <FontAwesomeIcon icon={faYoutube} className={'btn btn-outline-light btn-social'}/>
                    <FontAwesomeIcon icon={faInstagram} className={'btn btn-outline-light btn-social'}/>
                    <FontAwesomeIcon icon={faLinkedin} className={'btn btn-outline-light btn-social'}/>
                </div>
            </div>
        </>
    );
}

export default Get_In_Touch;