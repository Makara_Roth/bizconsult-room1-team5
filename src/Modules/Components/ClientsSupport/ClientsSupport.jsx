import React from 'react';

function ClientsSupport(props) {
    const {img} = props
    return (
        <>
            <div className={'container bg-primary p-4'}>
                <div className={"fs-3 d-flex justify-content-center"}>
                    <img src={img} width={"150px"} height={"150px"} ƒ alt={".."}/>
                </div>
            </div>
        </>

    );
}

export default ClientsSupport;