import React from 'react';
// Import Swiper React components
import {Swiper, SwiperSlide} from 'swiper/react';
// Import Swiper styles
import 'swiper/css';
import 'swiper/css/free-mode';
import ClientsSupport from "./ClientsSupport";
import  '../../../Assets/css/ClientsSupport/ClienteSupport.css'
// import required modules

import {Autoplay, FreeMode} from 'swiper/modules';

// Images Import
import CompanyLogo1 from "../../../Assets/img/company_logo/logo-1.png"
import CompanyLogo2 from "../../../Assets/img/company_logo/logo-2.png"
import CompanyLogo3 from "../../../Assets/img/company_logo/logo-3.png"
import CompanyLogo4 from "../../../Assets/img/company_logo/logo-4.png"
import CompanyLogo5 from "../../../Assets/img/company_logo/logo-5.png"
import CompanyLogo6 from "../../../Assets/img/company_logo/logo-6.png"
import CompanyLogo7 from "../../../Assets/img/company_logo/logo-7.png"
import CompanyLogo8 from "../../../Assets/img/company_logo/logo-8.png"


function ClientsSupportSwiper(props) {
    const Client = [
        {
            image : CompanyLogo1
        },
        {
            image: CompanyLogo2
        },
        {
            image: CompanyLogo3
        },
        {
            image: CompanyLogo4
        },
        {
            image: CompanyLogo5
        },
        {
            image: CompanyLogo6
        },
        {
            image: CompanyLogo7
        },
        {
            image: CompanyLogo8
        }
    ]

    return (
        <>
            <Swiper
                freeMode={true}
                loop={true}
                breakpoints={{
                    475: {
                        slidesPerView:2,
                    },
                    1024: {
                        slidesPerView:5,
                    },
                }}
                centeredSlides={true}
                autoplay={{
                    delay: 3000,
                    disableOnInteraction: false,
                    waitForTransition: true,
                }}
                modules={[FreeMode ,Autoplay]}
                className={'container bg-primary Slide'}
            >
                <>

                    {
                        Client.map((item, index) => {
                            return <SwiperSlide> <ClientsSupport key={index} img={item.image} /> </SwiperSlide>

                        })
                    }
                </>
            </Swiper>
        </>
    );
}

export default ClientsSupportSwiper;