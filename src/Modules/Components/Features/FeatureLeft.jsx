import React, {} from 'react'
import Buttons from "../../../Global/Button/Button";
import ButtonTitle from "../../../Global/Button/ButtonTitle";

const FeatureLeft = () => {
    return (
        <div data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
            <ButtonTitle name="Features" />
            <h2 className='mb-2'> Why People Choose Us? We Are Trusted & Award Wining Agency</h2>
            <div>
                <p>Clita noname sanctus nonumy et clita tempor, et sea amet ut et sadipscing rebum amet takimata amet,
                    sed accusam eos eos dolores dolore et. Et ea ea dolor rebum invidunt clita eos. Sea accusam stet
                    stet ipsum, sit ipsum et ipsum kasd</p>
                <p>Et ea ea dolor rebum invidunt clita eos. Sea accusam stet stet ipsum, sit ipsum et ipsum kasd</p>
            </div>
            <Buttons name={"Learn More"}/>
        </div>
    )
}

export default FeatureLeft