import 'bootstrap/dist/css/bootstrap.min.css';
import FeatureLeft from './FeatureLeft';
import FeatureRight from './FeatureRight';
import '../../../Assets/css/Feature/Feature.css'

const Feature = () => {
    return (
        <>
            <div className='container ' style={{marginTop: "10rem" ,marginBottom:"10rem"}}>
                <div className='row'>
                    <div className='col-lg-5'>
                        <FeatureLeft/>
                    </div>
                    <div className='col-lg-7'>
                        <FeatureRight/>
                    </div>
                </div>
            </div>
        </>
    )
}
export default Feature;