
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faCubes , faPercent , faAward ,faSmileBeam , faUserTie , faHeadset } from '@fortawesome/free-solid-svg-icons'


const FeatureRight = () => {
  return (
    <div className='container mt-3'>
      <div className='row'>
        <div className=' col-sm-6' data-aos="zoom-out-up">
          <div className=' d-flex align-item-center mb-3'>
            <div className=" btn-square flex-shrink-0 text-light bg-primary p-2 m-1">
              <FontAwesomeIcon icon={faCubes} style={{fontSize:"14px "}} />
            </div>
            <h6 className="mt-3 ms-3 fs-5 fw-bold"> Best In Industry</h6>
          </div>
            <p style={{fontSize:"14px"}}> Magna sea eos sit dolor, ipsum amet ipsum lorem diam eos diam dolor</p>
        </div>
        <div className=' col-sm-6' data-aos="zoom-out-up">
          <div className=' d-flex align-item-center mb-3'>
            <div className="btn-square flex-shrink-0 p-3 text-light bg-primary rounded-circle m-1">
              <FontAwesomeIcon icon={faPercent} style={{fontSize:"14px "}} />
            </div>
            <h6 className="mt-3 ms-3 fs-5 fw-bold"> Best In Industry</h6>
          </div>
            <p style={{fontSize:"14px"}}> Magna sea eos sit dolor, ipsum amet ipsum lorem diam eos diam dolor</p>
        </div>
        <div className=' col-sm-6' data-aos="zoom-out-up">
          <div className=' d-flex align-item-center mb-3'>
            <div className="btn-square  flex-shrink-0 p-3 text-light bg-primary rounded-circle m-1">
              <FontAwesomeIcon icon={faAward} style={{fontSize:"14px "}} />
            </div>
            <h6 className="mt-3 ms-3 fs-5 fw-bold"> Best In Industry</h6>
          </div>
            <p style={{fontSize:"14px"}}> Magna sea eos sit dolor, ipsum amet ipsum lorem diam eos diam dolor</p>
        </div>
        <div className=' col-sm-6' data-aos="zoom-out-up">
          <div className=' d-flex align-item-center mb-3'>
            <div className="btn-square flex-shrink-0 p-3 text-light bg-primary rounded-circle m-1">
              <FontAwesomeIcon icon={faSmileBeam} style={{fontSize:"14px "}} />
            </div>
            <h6 className="mt-3 ms-3 fs-5 fw-bold"> Best In Industry</h6>
          </div>
            <p style={{fontSize:"14px"}}> Magna sea eos sit dolor, ipsum amet ipsum lorem diam eos diam dolor</p>
        </div>
        <div className=' col-sm-6' data-aos="zoom-out-up">
          <div className=' d-flex align-item-center mb-3'>
            <div className="btn-square  flex-shrink-0 p-3 text-light bg-primary rounded-circle m-1">
              <FontAwesomeIcon icon={faUserTie} style={{fontSize:"14px "}} />
            </div>
            <h6 className="mt-3 ms-3 fs-5 fw-bold"> Best In Industry</h6>
          </div>
            <p style={{fontSize:"14px"}}> Magna sea eos sit dolor, ipsum amet ipsum lorem diam eos diam dolor</p>
        </div>
        <div className=' col-sm-6' data-aos="zoom-out-up">
          <div className=' d-flex align-item-center mb-3'>
            <div className="btn-square  flex-shrink-0 p-3 text-light bg-primary rounded-circle m-1">
              <FontAwesomeIcon icon={faHeadset} style={{fontSize:"14px "}} />
            </div>
            <h6 className="mt-3 ms-3 fs-5 fw-bold"> Best In Industry</h6>
          </div>
            <p style={{fontSize:"14px"}}> Magna sea eos sit dolor, ipsum amet ipsum lorem diam eos diam dolor</p>
        </div>
      </div>
    </div>
  )
}

export default FeatureRight