
// NotFoundPage.js

import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTriangleExclamation } from '@fortawesome/free-solid-svg-icons';
import Button from 'react-bootstrap/Button';

const ErrorPages = () => {

  const styles = {
    container: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      height: '100vh',
      fontFamily: 'Arial, sans-serif',
      textAlign: 'center',
    },
    heading: {
      fontSize: '4em',
      marginBottom: '10px',
      color: '#333',
    },
    text: {
      fontSize: '1.5em',
      color: '#666',
    },
  };

  return (
    <>
    <div style={styles.container}>
      <FontAwesomeIcon icon={faTriangleExclamation} style={{fontSize:'5rem',color:'red'}}/>
      <h1 style={styles.heading}>404</h1>
      <h1 style={styles.heading}>Page Not Found</h1>
      <p style={styles.text}>Sorry, the page you are looking for might be in another castle.</p>
      <Button className="rounded-pill" variant="success" size="lg" active>
        Go Back To Home
      </Button>
    </div>
    </>
  );
};



export default ErrorPages;
