import React from 'react';
import Button from "../../../Global/Button/Button";
import "../../../Assets/css/Hero/Hero.css"
import HeroS from "../../../Assets/img/hero.png"


function Hero() {
    return (
        <>
            <div className={'container bg-primary hero'}>
                <div className={'row g-5 align-items-center mx-2'}>
                    <div className={'col-lg-6 text-center text-lg-start text-light'}>
                        <h1 className={'text-light fw-bold '}> We Top To Push Your Business To The Top Level </h1>
                        <p>Tempor rebum no at dolore lorem clita rebum rebum ipsum rebum stet dolor sed justo kasd. Ut
                            dolor sed magna dolor sea diam. Sit diam sit justo amet ipsum vero ipsum clita lorem</p>
                        <Button name={"Learn More"}/>
                    </div>
                    <div className={'col-lg-6'}>
                        <img src={HeroS} width={"100%"} alt={''}/>
                    </div>

                </div>
            </div>

        </>
    );
}

export default Hero;


