import React from 'react';
import Card from "react-bootstrap/Card";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {Col, Row} from "react-bootstrap";

function TestimonialCard(props) {
    const { CardText ,img , icon } = props

    return (
       <>
           <Card  style={{width: '100%'}} className={'CardItem'}>
               <Card.Body className={'testimonial-item '}>
                   <FontAwesomeIcon className={"fa-2x text-primary mb-3"} icon={icon}/>
                   <Card.Text style={{color:"#666565"}} >
                       {CardText}
                   </Card.Text>
                  <Row>
                      <Col lg={3} md={6} sm={6}  >
                   <Card.Img  variant="top" style={{height: "4rem", width: "4rem", borderRadius: "50%"}}
                             src={img}/>
                      </Col>
                      <Col lg={9} md={6} sm={6} className={'mt-2'}>
                   <div className={'d-inline'}>
                       <h6> Client Name </h6>
                       <p> Profession </p>
                   </div>
                      </Col>
                  </Row>
               </Card.Body>
           </Card>
       </>
    );
}

export default TestimonialCard;