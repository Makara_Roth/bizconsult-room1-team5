//====
import {Pagination, EffectCoverflow, Navigation, Autoplay} from 'swiper/modules';
import {Swiper, SwiperSlide} from 'swiper/react';
import 'swiper/css';
import 'swiper/css/navigation';
// =====
import {faQuoteLeft, faChevronRight, faChevronLeft} from "@fortawesome/free-solid-svg-icons";
import {Button} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
// =====
import '../../Pages/Testimonial/Testimonial.css'
import TestimonialCard from "./TestimonialCard";
import testimonial1 from "../../../Assets/img/Testimonial/testimonial-1.jpg";
import testimonial2 from "../../../Assets/img/Testimonial/testimonial-2.jpg";
import testimonial3 from "../../../Assets/img/Testimonial/testimonial-3.jpg";
import testimonial4 from "../../../Assets/img/Testimonial/testimonial-4.jpg";


function SwapperTestimonial(props) {

    const lists = [
        {
            icon: faQuoteLeft,
            CardText: "Dolor et eos labore, stet justo sed est sed. Diam sed sed dolor stet amet eirmod eos labore diam",
            img: testimonial1,
        },
        {
            icon: faQuoteLeft,
            CardText: "Dolor et eos labore, stet justo sed est sed. Diam sed sed dolor stet amet eirmod eos labore diam",
            img: testimonial2
        },
        {
            icon: faQuoteLeft,
            CardText: "Dolor et eos labore, stet justo sed est sed. Diam sed sed dolor stet amet eirmod eos labore diam",
            img: testimonial3
        },
        {
            icon: faQuoteLeft,
            CardText: "Dolor et eos labore, stet justo sed est sed. Diam sed sed dolor stet amet eirmod eos labore diam",
            img: testimonial4
        },
        {
            icon: faQuoteLeft,
            CardText: "Dolor et eos labore, stet justo sed est sed. Diam sed sed dolor stet amet eirmod eos labore diam",
            img: testimonial1
        },
        {
            icon: faQuoteLeft,
            CardText: "Dolor et eos labore, stet justo sed est sed. Diam sed sed dolor stet amet eirmod eos labore diam",
            img: testimonial2
        }
    ]
    return (
        <div className={'container'}>
            <Swiper
                loop={true}
                centeredSlides={true}
                breakpoints={{
                    475: {
                        slidesPerView: 1,
                    },
                    768: {
                        slidesPerView: 2,
                        spaceBetween: 10,
                    },
                    1024: {
                        slidesPerView: 3,
                        spaceBetween: 20,
                    },
                }}
                autoplay={{
                    delay: 3000,
                    disableOnInteraction: false,
                    waitForTransition: true,
                }}
                navigation={{
                    prevEl: '.prev',
                    nextEl: '.next',
                }}
                watchSlidesProgress={true}
                effect={'coverflow'}
                grabCursor={true}
                coverflowEffect={{
                    rotate: 1,
                    stretch: 0,
                    depth: 1,
                    modifier: 1,
                }}
                modules={[EffectCoverflow, Pagination, Navigation, Autoplay]}

            >
                <>

                    {
                        lists.map((item, index) => {
                            return <SwiperSlide>
                                <TestimonialCard key={index} CardText={item.CardText} img={item.img} icon={item.icon}/>
                            </SwiperSlide>

                        })
                    }
                </>
                <div className={'d-flex justify-content-center m-2 '}>
                    <Button className={"prev"}><FontAwesomeIcon icon={faChevronLeft}/></Button>
                    <Button className={"next"}><FontAwesomeIcon icon={faChevronRight}/></Button>
                </div>
            </Swiper>
        </div>
    );
}

export default SwapperTestimonial;