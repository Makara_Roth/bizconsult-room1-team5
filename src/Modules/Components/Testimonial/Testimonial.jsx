import ButtonTitle from "../../../Global/Button/ButtonTitle";
import SwapperTestimonial from "./SwapperTestimonial";
import {Title} from "../../../Global/Title/Title";

const Testimonial = () => {
    return (
        <>
            <div className={"container"}  data-aos="fade-up" data-aos-duration="1500">
                <div className={"d-flex justify-content-center my-1 mt-5  "}>
                    <ButtonTitle name={"Testimonial"}/>
                </div>
                <div className={'text-center text-dark mt-3'}>
                    <Title title="What Our Clients Say!"/>
                </div>
                <SwapperTestimonial/>
            </div>
        </>
    )
}
export default Testimonial  
