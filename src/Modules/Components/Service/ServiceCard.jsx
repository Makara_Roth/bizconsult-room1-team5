import { Col, Container, Row } from "react-bootstrap";
import ButtonTitle from "../../../Global/Button/ButtonTitle";
import { Card } from "./Card";
import { Title } from "../../../Global/Title/Title";
import {
  faUserTie,
  faChartPie,
  faChartLine,
  faChartArea,
  faBalanceScale,
  faHouseDamage,
} from "@fortawesome/free-solid-svg-icons";

const ServiceCard = () => {
  const listservice = [
    {
      title: "Business Research",
      subtitle:
        "Erat ipsum justo amet duo et elitr dolor, est duo duo eos lorem sed diam stet diam sed stet lorem.",
      icon: faUserTie,
    },
    {
      title: "Stretagic Planning",
      subtitle:
        "Erat ipsum justo amet duo et elitr dolor, est duo duo eos lorem sed diam stet diam sed stet lorem.",
      icon: faChartPie,
    },
    {
      title: "Market Analysis",
      subtitle:
        "Erat ipsum justo amet duo et elitr dolor, est duo duo eos lorem sed diam stet diam sed stet lorem.",
      icon: faChartLine,
    },
    {
      title: "Financial Analaysis",
      subtitle:
        "Erat ipsum justo amet duo et elitr dolor, est duo duo eos lorem sed diam stet diam sed stet lorem.",
      icon: faChartArea,
    },
    {
      title: "legal Advisory",
      subtitle:
        "Erat ipsum justo amet duo et elitr dolor, est duo duo eos lorem sed diam stet diam sed stet lorem.",
      icon: faBalanceScale,
    },
    {
      title: "Tax & Insurance",
      subtitle:
        "Erat ipsum justo amet duo et elitr dolor, est duo duo eos lorem sed diam stet diam sed stet lorem.",
      icon: faHouseDamage,
    },
  ];

  return (
    <Container style={{ marginTop: "9rem" }}>
      <div data-aos="fade-up" data-aos-duration="1500">
        <div className="d-flex justify-content-center mt-5">
          <ButtonTitle name="Our Services" />
        </div>
        <div className="text-center mx-auto" style={{ maxWidth: "500px" }}>
          <Title title="We Provide Solutions On Your Business" />
        </div>
      </div>
      <Row className="g-4">
        {listservice.map((res, index) => {
          return (
            <Col md={6} lg={4} key={index}>
              <Card title={res.title} subtitle={res.subtitle} icon={res.icon} />
            </Col>
          );
        })}
      </Row>
    </Container>
  );
};
export default ServiceCard;
