import React from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

export const Card = (props) => {
  const {title, subtitle, icon} = props

  return (
    <div
      className="service-card rounded"
      data-aos="fade-up"
      data-aos-duration="1500"
    >
      <div className="d-flex justify-content-between">
        <div >
          <FontAwesomeIcon className="service-icon" icon={icon}/>
        </div>
        <Link to="" className="service-link text-decoration-none">
          <i className="fa fa-link fa-2x"></i>
        </Link>
      </div>
      <div className="p-5">
        <h5 className="mb-3">{title}</h5>
        <p>
          {subtitle}
        </p>
      </div>
    </div>
  );
};
