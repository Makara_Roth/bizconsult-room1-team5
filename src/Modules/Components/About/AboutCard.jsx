import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import about from '../../../Assets/img/about.png'
import Buttons from '../../../Global/Button/Button'
import ButtonTitle from '../../../Global/Button/ButtonTitle'
import { Title } from '../../../Global/Title/Title'

const AboutCard = () => {
  return (
    <Container className='py-5' style={{marginTop: "9rem"}}>
      <Row>   
        <Col md={6}>
          <div data-aos="zoom-in" data-aos-duration="1500">
            <img src={about} alt="about" style={{width: "80%"}}/>
          </div>
        </Col>
        <Col md={6}>
          <div data-aos="fade-up"
     data-aos-duration="1500">
          <ButtonTitle name="About Us"/>
          <Title title="Award Wining Consultancy Agency For Your Business"/>
          <p className="mb-4">Tempor erat elitr rebum at clita. Diam dolor diam ipsum et tempor sit. Aliqu diam amet diam et eos labore. Clita erat ipsum et lorem et sit, sed stet no labore lorem sit. Sanctus clita duo justo et tempor eirmod</p>
          <Row className='g-3 mb-4'>
            <Col md={12} >
              <div className='d-flex'>
                <div className="flex-shrink-0 bg-primary rounded-circle icon-lg d-flex justify-content-center align-items-center">
                <i className="fa fa-user-tie"></i>
                </div>
                <div className='ms-4'>
                  <h6>Business Planing</h6>
                  <span>Tempor erat elitr rebum at clita. Diam dolor ipsum amet eos erat ipsum lorem et sit sed stet lorem sit clita duo</span>
                </div>
              </div>
            </Col>
            <Col md={12}>
              <div className='d-flex'>
                <div className="flex-shrink-0 bg-primary rounded-circle icon-lg d-flex justify-content-center align-items-center">
                <i className="fa fa-chart-line"></i>
                </div>
                <div className='ms-4'>
                  <h6>Financial Analaysis</h6>
                  <span>Tempor erat elitr rebum at clita. Diam dolor ipsum amet eos erat ipsum lorem et sit sed stet lorem sit clita duo</span>
                </div>
              </div>
              <Buttons/>
            </Col>
          </Row>
          </div>
        </Col>
      </Row>
    </Container>
  )
}

export default AboutCard
