import Card from "react-bootstrap/Card";
import image1 from "../../../Assets/img/team/team-1.jpg";
import image2 from "../../../Assets/img/team/team-2.jpg";
import image3 from "../../../Assets/img/team/team-3.jpg";
import image4 from "../../../Assets/img/team/team-4.jpg";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faFacebookF,
    faTwitter,
    faLinkedinIn,
} from "@fortawesome/free-brands-svg-icons";
import ButtonTitle from "../../../Global/Button/ButtonTitle";

const OurTeamCard = () => {
    return (
        <>
            <div className="container">
                <div className="team mx-auto text-center" data-aos="fade-up">
                    <ButtonTitle name="Our Team"/>
                    <h2 className="mb-5">Meet Our Team Members</h2>
                    <div className="row g-4 d-flex justify-content-center align-items-center">
                        <div
                            className="col-lg-3 col-md-6 col-sm-12 team-item"
                            data-aos="fade-up"
                            data-aos-duration="1000"
                            data-aos-delay="0.1"
                        >
                            <Card
                                className="d-flex justify-content-center align-items-center shadow-sm"
                            >
                                <Card.Text>
                                    <h5 className="mt-4 fw-bold">Full Name</h5>
                                    <p>Designation</p>
                                </Card.Text>
                                <div className="wrapper">
                                    <img src={image1} alt="__"/>
                                </div>
                                <Card.Body>
                                    <div className="icon d-flex justify-content-center">
                                        <FontAwesomeIcon
                                            className="mx-3 m-1 fa-lg"
                                            icon={faFacebookF}
                                        />
                                        <FontAwesomeIcon className="mx-3 m-1 fa-lg" icon={faTwitter}/>
                                        <FontAwesomeIcon
                                            className="mx-3 m-1 fa-lg"
                                            icon={faLinkedinIn}
                                        />
                                    </div>
                                </Card.Body>
                            </Card>
                        </div>
                        <div
                            className="col-lg-3 col-md-6 col-sm-12 team-item"
                            data-aos="fade-up"
                            data-aos-duration="1300"
                            data-aos-delay="0.1"
                        >
                            <Card
                                className="d-flex justify-content-center align-items-center shadow-sm"
                            >
                                <Card.Text>
                                    <h5 className="mt-4 fw-bold">Full Name</h5>
                                    <p className="mb-4">Designation</p>
                                </Card.Text>
                                <div className="wrapper">
                                    <img src={image2} alt="__"/>
                                </div>
                                <Card.Body>
                                    <div className="icon d-flex justify-content-center">
                                        <FontAwesomeIcon
                                            className="mx-3 m-1 fa-lg"
                                            icon={faFacebookF}
                                        />
                                        <FontAwesomeIcon className="mx-3 m-1 fa-lg" icon={faTwitter}/>
                                        <FontAwesomeIcon
                                            className="mx-3 m-1 fa-lg"
                                            icon={faLinkedinIn}
                                        />
                                    </div>
                                </Card.Body>
                            </Card>
                        </div>
                        <div
                            className="col-lg-3 col-md-6 col-sm-12 team-item"
                            data-aos="fade-up"
                            data-aos-duration="1600"
                            data-aos-delay="0.1"
                        >
                            <Card
                                className="d-flex justify-content-center align-items-center shadow-sm"
                            >
                                <Card.Text>
                                    <h5 className="mt-4 fw-bold">Full Name</h5>
                                    <p className="mb-4">Designation</p>
                                </Card.Text>
                                <div className="wrapper">
                                    <img src={image3} alt="__"/>
                                </div>
                                <Card.Body>
                                    <div className="icon d-flex justify-content-center">
                                        <FontAwesomeIcon
                                            className="mx-3 m-1 fa-lg"
                                            icon={faFacebookF}
                                        />
                                        <FontAwesomeIcon className="mx-3 m-1 fa-lg" icon={faTwitter}/>
                                        <FontAwesomeIcon
                                            className="mx-3 m-1 fa-lg"
                                            icon={faLinkedinIn}
                                        />
                                    </div>
                                </Card.Body>
                            </Card>
                        </div>
                        <div
                            className="col-lg-3 col-md-6 col-sm-12 team-item"
                            data-aos="fade-up"
                            data-aos-duration="1900"
                            data-aos-delay="0.1"
                        >
                            <Card
                                className="d-flex justify-content-center align-items-center shadow-sm"
                            >
                                <Card.Text>
                                    <h5 className="mt-4 fw-bold">Full Name</h5>
                                    <p className="mb-4">Designation</p>
                                </Card.Text>
                                <div className="wrapper">
                                    <img src={image4} alt="__"/>
                                </div>
                                <Card.Body>
                                    <div className="icon d-flex justify-content-center">
                                        <FontAwesomeIcon
                                            className="mx-3 m-1 fa-lg"
                                            icon={faFacebookF}
                                        />
                                        <FontAwesomeIcon className="mx-3 m-1 fa-lg" icon={faTwitter}/>
                                        <FontAwesomeIcon
                                            className="mx-3 m-1 fa-lg"
                                            icon={faLinkedinIn}
                                        />
                                    </div>
                                </Card.Body>
                            </Card>
                        </div>
                        <div
                            className="col-lg-3 col-md-6 col-sm-12 team-item"
                            data-aos="fade-up"
                            data-aos-duration="1000"
                            data-aos-delay="0.1"
                        >
                            <Card
                                className="d-flex justify-content-center align-items-center shadow-sm"
                            >
                                <Card.Text>
                                    <h5 className="mt-4 fw-bold">Full Name</h5>
                                    <p className="mb-4">Designation</p>
                                </Card.Text>
                                <div className="wrapper">
                                    <img src={image2} alt="__"/>
                                </div>
                                <Card.Body>
                                    <div className="icon d-flex justify-content-center">
                                        <FontAwesomeIcon
                                            className="mx-3 m-1 fa-lg"
                                            icon={faFacebookF}
                                        />
                                        <FontAwesomeIcon className="mx-3 m-1 fa-lg" icon={faTwitter}/>
                                        <FontAwesomeIcon
                                            className="mx-3 m-1 fa-lg"
                                            icon={faLinkedinIn}
                                        />
                                    </div>
                                </Card.Body>
                            </Card>
                        </div>
                        <div
                            className="col-lg-3 col-md-6 col-sm-12 team-item"
                            data-aos="fade-up"
                            data-aos-duration="1300"
                            data-aos-delay="0.1"
                        >
                            <Card
                                className="d-flex justify-content-center align-items-center shadow-sm"
                            >
                                <Card.Text>
                                    <h5 className="mt-4 fw-bold">Full Name</h5>
                                    <p className="mb-4">Designation</p>
                                </Card.Text>
                                <div className="wrapper">
                                    <img src={image3} alt="__"/>
                                </div>
                                <Card.Body>
                                    <div className="icon d-flex justify-content-center">
                                        <FontAwesomeIcon
                                            className="mx-3 m-1 fa-lg"
                                            icon={faFacebookF}
                                        />
                                        <FontAwesomeIcon className="mx-3 m-1 fa-lg" icon={faTwitter}/>
                                        <FontAwesomeIcon
                                            className="mx-3 m-1 fa-lg"
                                            icon={faLinkedinIn}
                                        />
                                    </div>
                                </Card.Body>
                            </Card>
                        </div>
                        <div
                            className="col-lg-3 col-md-6 col-sm-12 team-item"
                            data-aos="fade-up"
                            data-aos-duration="1600"
                            data-aos-delay="0.1"
                        >
                            <Card
                                className="d-flex justify-content-center align-items-center shadow-sm"
                            >
                                <Card.Text>
                                    <h5 className="mt-4 fw-bold">Full Name</h5>
                                    <p className="mb-4">Designation</p>
                                </Card.Text>
                                <div className="wrapper">
                                    <img src={image4} alt="__"/>
                                </div>
                                <Card.Body>
                                    <div className="icon d-flex justify-content-center">
                                        <FontAwesomeIcon
                                            className="mx-3 m-1 fa-lg"
                                            icon={faFacebookF}
                                        />
                                        <FontAwesomeIcon className="mx-3 m-1 fa-lg" icon={faTwitter}/>
                                        <FontAwesomeIcon
                                            className="mx-3 m-1 fa-lg"
                                            icon={faLinkedinIn}
                                        />
                                    </div>
                                </Card.Body>
                            </Card>
                        </div>
                        <div
                            className="col-lg-3 col-md-6 col-sm-12 team-item"
                            data-aos="fade-up"
                            data-aos-duration="1900"
                            data-aos-delay="0.1"
                        >
                            <Card
                                className="d-flex justify-content-center align-items-center shadow-sm"
                            >
                                <Card.Text>
                                    <h5 className="mt-4 fw-bold">Full Name</h5>
                                    <p className="mb-4">Designation</p>
                                </Card.Text>
                                <div className="wrapper">
                                    <img src={image1} alt="__"/>
                                </div>
                                <Card.Body>
                                    <div className="icon d-flex justify-content-center">
                                        <FontAwesomeIcon
                                            className="mx-3 m-1 fa-lg"
                                            icon={faFacebookF}
                                        />
                                        <FontAwesomeIcon className="mx-3 m-1 fa-lg" icon={faTwitter}/>
                                        <FontAwesomeIcon
                                            className="mx-3 m-1 fa-lg"
                                            icon={faLinkedinIn}
                                        />
                                    </div>
                                </Card.Body>
                            </Card>
                        </div>
                    </div>
                </div>
            </div>
        </>

    );
};
export default OurTeamCard;
